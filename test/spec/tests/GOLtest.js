(function () {
    describe("Game of Life", function () {

        var game;

        describe("GOL class Constructor", function () {

            it("takes the right dimensions",
                function () {
                    game = new GOL(6, 5);
                    let result = { rows: game.rows, cols: game.cols };
                    let expected = { rows: 6, cols: 5 };
                    expect(result).toEqual(expected);
                });
        });

        describe("GOL class setAlive", function () {

            beforeEach(function() {
                game = new GOL(3, 3);
            });

            it("setAlive gives life",
                function () {
                    game.setAlive({ rowIndex:1, cellIndex:0 });
                    expect(game.currentGrid[1][0]).toBeTruthy();
                });
                
                it("setAlive takes life",
                function () {
                    game.setAlive({ rowIndex:1, cellIndex:0 });
                    game.setAlive({ rowIndex:1, cellIndex:0 });
                    expect(game.currentGrid[1][0]).toBeFalsy();
                });
            });
            
        describe("GOL class 'aliveNeighboursCounter' method", function () {

            it("aliveNeighboursCounter gives the right result",
                function () {
                    game = new GOL(2, 2);
                    let result = game.aliveNeighboursCounter({
                        array: [
                            [false, false, false],
                            [true, false, false],
                            [false, false, false],
                        ],
                        rows: 1, cols: 1
                    });
                    let expected = 1;
                    expect(result).toEqual(expected);
                });

            it("aliveNeighboursCounter gives the right result at blinker array",
                function () {
                    game = new GOL(5, 5);
                    let array = Tools.getArray({ name: 'blinker' });
                    let result = game.aliveNeighboursCounter({
                        array: array,
                        rows: 2, cols: 2
                    });
                    let expected = 3;
                    expect(result).toEqual(expected);
                });

            it("aliveNeighboursCounter gives the right result at glider array",
                function () {
                    game = new GOL(6, 6);
                    let array = Tools.getArray({ name: 'glider' });
                    let result = game.aliveNeighboursCounter({
                        array: array,
                        rows: 3, cols: 3
                    });
                    let expected = 5;
                    expect(result).toEqual(expected);
                });
        });

        describe("GOL class 'step' method", function () {

            it("array gives the right result",
                function () {
                    game = new GOL(3, 3);
                    game.currentGrid = [
                        [false, false, false],
                        [true, false, false],
                        [false, false, false]
                    ];
                    game.step();
                    let expected = [
                        [false, false, false],
                        [false, false, false],
                        [false, false, false]
                    ];
                    expect(game.currentGrid).toEqual(expected);
                });

            it("blinker array gives the right result",
                function () {
                    game = new GOL(5, 5);
                    game.currentGrid = Tools.getArray({ name: 'blinker' });
                    game.step();
                    let expected = Tools.getArray({ name: 'blinkerReturn' });
                    expect(game.currentGrid).toEqual(expected);
                });

            it("glider array gives the right result",
                function () {
                    game = new GOL(6, 6);
                    game.currentGrid = Tools.getArray({ name: 'glider' });
                    game.step();
                    let expected = Tools.getArray({ name: 'gliderReturn' });
                    expect(game.currentGrid).toEqual(expected);
                });
        })
    })
})();
