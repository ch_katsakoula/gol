window.Tools = {};

(function (Tools) {

    let PatternsArrays = {
        glider: [
           [ false, false, false, false, false, false,],
           [ false, false, false, false, false, false,],

            [false, false, true, true, true, false],
            [false, true, true, true, false, false],

            [false, false, false, false, false, false],
            [false, false, false, false, false, false],
        ],
        gliderReturn: [
            [false, false, false, false, false, false,],
            [false, false, false, true, false, false,],

            [false, true, false, false, true, false,],
            [false, true, false, false, true, false,],

            [false, false, true, false, false, false],
            [false, false, false, false, false, false],
        ],

        blinker: [
            [false, false, false, false, false],
            [false, false, true, false, false],
            [false, false, true, false, false],
            [false, false, true, false, false],
            [false, false, false, false, false],
        ],
        blinkerReturn: [
            [false, false, false, false, false],
            [false, false, false, false, false],
            [false, true, true, true, false],
            [false, false, false, false, false],
            [false, false, false, false, false]
        ]
    };

    Tools.getArray = function ({ name }) {
        return PatternsArrays[name];
    }

    return Tools;
})(window.Tools || {});