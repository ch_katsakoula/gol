window.Main = {};
(function (Main) {
    Main.UI = Main.UI || {};

    let compileNewGeneration = function ({ index, nextGeneration }) {
        var source = $('#new-generation').html();
        var template = Handlebars.compile(source);

        let game = window.games[index].game;
        var data = Main.Tools.handlebarsFormater({ index, array: nextGeneration, rows: game.rows, cols: game.cols });

        return template(data);
    }

    let compileNewGrid = function ({ index }) {

        var source = $('#grid-creator').html();
        var template = Handlebars.compile(source);

        var data = { index, row: [compileNewGeneration({ index })] };

        return template(data);
    }

    Main.UI.displayTemplate = function ({ index, nextGeneration }) {
        if (nextGeneration){
            console.log(nextGeneration);
            $('[name="' + index + '"]').find('.table').html(compileNewGeneration({ index, nextGeneration }));
        }
        else
            document.getElementById('grid-container').innerHTML += compileNewGrid({ index });
    }

    return Main;
})(window.Main || {});