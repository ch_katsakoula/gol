class GOL {

    constructor(rows, cols) {
        this.rows = rows;
        this.cols = cols;
        this.currentGrid = this.createGrid(this.rows, this.cols);
    }

    createGrid() {

        let array = [];
        for (let x = 0; x < this.rows; x++) {
            const row = [];
            for (let y = 0; y < this.cols; y++) {
                row.push(false);
            }
            array.push(row);
        }
        return array;
    }

    setAlive({ rowIndex, cellIndex }) {
        this.currentGrid[rowIndex][cellIndex] = !this.currentGrid[rowIndex][cellIndex];
    }


    aliveNeighboursCounter({ array: array, rows: x, cols: y }) {

        let counter = 0;
        let positions = [-1, 0, 1];
        for (let i = 0; i < positions.length; i++) {

            let index = x - positions[i];

            if (array[index]) {
                if (array[index][y - 1])
                    counter++;
                if (array[index][y])
                    counter++;
                if (array[index][y + 1])
                    counter++;
            }
        }
        return counter;
    }

    step() {
        let nextGenerationArray = [];
        for (let x = 0; x < this.currentGrid.length; x++) {

            let row = [];
            for (let y = 0; y < this.currentGrid[x].length; y++) {

                let lifeCounter = this.aliveNeighboursCounter({ array: this.currentGrid, rows: x, cols: y });
                // aliveNeighboursCounter function adds one to the lifeCounter for every alive cell
                // (current cell included, not only neighbours) we need to remove one life, if the current cell is alive
                if (this.currentGrid[x][y])
                    lifeCounter--;
                // end

                // create the next generation
                if (lifeCounter === 3 || lifeCounter === 2) {
                    if (this.currentGrid[x][y] === true || lifeCounter === 3)
                        row.push(true);
                    else
                        row.push(false);
                }
                else
                    row.push(false);
            }
            nextGenerationArray.push(row);
        }
        this.currentGrid = nextGenerationArray;
    }

}