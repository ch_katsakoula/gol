$(document).ready(function () {

    window.games = [];
    window.gameIndex = 0;

    $('#createGrid').on('click', function () {
        let rows = $('#rows').val();
        let cols = $('#cols').val();

        if (rows && cols && typeof parseInt(rows) === 'number' && typeof parseInt(cols) === 'number') {
            window.games.push({ index: window.gameIndex, game: new GOL(rows, cols) });
            Main.UI.displayTemplate({ index: window.gameIndex });
            window.gameIndex++;
        }
        else
            window.alert('Fill with numbers!');
    });


    $('#grid-container').on('click', 'td', function () {
        $('#start, #stop, #clear, #step').prop('disabled', false);
        let tableID = $(this).closest(".game-container").attr('name');
        if (!(window.games[tableID].game.timerId)) {
            window.games[tableID].game.setAlive({ rowIndex: $(this).parent().index(), cellIndex: $(this).index() });
            $(this).attr('class', this.className == 'life' ? 'death' : 'life')
        }
    });

    $('#grid-container').on('click', '#step', function () {
        let tableID = $(this).closest(".game-container").attr('name');
        if (!(window.games[tableID].game.timerId)) {
            window.games[tableID].game.step();
            Main.UI.displayTemplate({ index: tableID, nextGeneration: window.games[tableID].game.currentGrid });
        }
    });

    $('#grid-container').on('click', '#start', function () {
     
        $('td, #start, #step').prop('disabled', true);
     
        let tableID = $(this).closest(".game-container").attr('name');

        if (!(window.games[tableID].game.timerId)) {
            window.games[tableID].game.timerId = setInterval(() => {
                window.games[tableID].game.step();
                Main.UI.displayTemplate({ index: tableID, nextGeneration: window.games[tableID].game.currentGrid });
            }, 650)
        }
    });

    $('#grid-container').on('click', '#stop', function () {
        
        $('td, #start, #step').prop('disabled', false);

        let tableID = $(this).closest(".game-container").attr('name');
        if (window.games[tableID].game.timerId) {
            clearInterval(window.games[tableID].game.timerId);
            window.games[tableID].game.timerId = null;
        }
    });

    $('#grid-container').on('click', '#clear', function () {

        $('td').prop('disabled', false);
        $('#start, #step, #clear, #stop').prop('disabled', true);


        let index = $(this).closest(".game-container").attr('name');
        if (window.games[index].game.timerId) {
            clearInterval(window.games[index].game.timerId);
            window.games[index].game.timerId = null;
        }
        window.games[index].game.currentGrid = window.games[index].game.createGrid();
        Main.UI.displayTemplate({ index, nextGeneration: window.games[index].game.currentGrid });
    });

    $('#grid-container').on('click', '#delete', function () {
        let tableID = $(this).closest(".game-container").remove().attr('name');
        if (window.games[tableID].game.timerId)
            clearInterval(window.games[tableID].game.timerId);
        window.games[tableID] = null;
    });

});