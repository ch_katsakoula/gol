(function (Main) {
    Main.Tools = Main.Tools || {};

    Main.Tools.handlebarsFormater = function ({ index, array, rows, cols }) {

        var data = { index, row: [] };
        for (let x = 0; x < rows; x++) {
            let columns = [];
            for (let y = 0; y < cols; y++) {
                if (array)
                    columns.push(array[x][y]);
                else
                    columns.push(false);
            }
            data.row.push({ cols: columns });
        }
        return data;
    }
    return Main;
})(window.Main || {});